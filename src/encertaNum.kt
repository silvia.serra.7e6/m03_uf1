import java.util.*

fun main () {
    val tope = 20
    val aleatori = (0..tope).random()
    println("Número a adivinar: $aleatori\n\n")
    val entrada = Scanner(System.`in`)
    var correcte = false
    var intents= 3
    var num: Int

    do{

        do{
            print("Ingrese un numero entre 1 y $tope: ")
            num = entrada.next().toInt()
        }while((num < 0)  || (num > tope))

        intents--

        if (num == aleatori) correcte=true

        if (!correcte){
            if(num > aleatori){
                println("El número a adivinar es menor")
            } else {
                println("El número a adivinar es mayor")
            }
        } else {
            println (" ACERTASTE!! ")
        }

    } while ((!correcte ) && (intents >0))

    println("Se acabó!")
}