## PRODUCTE: WORDLE

El producte Wordle està programat en Kotlin i desat en format .kt. S'ha implementat correctament en castellà per tal d'abarcar a més mercat i tenir més quantitat de jugadors.

Els passos a seguir han sigut:

- Triar un diccionari vàlid.
- Triar una paleta de colors tant de lletra com de fons.
- Programar el joc.
- Testejar el joc.

Les instruccions del joc:

- L'usuari ha d'introduir una paraula de 5 lletres que haurà d'encertar. En cas contrari es tornarà a demanar d'escriure una paraula.
- La partida consta de 6 intents on s'anirà restant cada intent quan s'escriu una paraula.
- Després de cada proposta les lletres apareixen en colors:
  + El fons gris representa les lletres que no estan a la paraula cercada.
  + El fons groc representa les lletres que es troben en altres llocs de la paraula.
  + El fons verd representa les lletres que estan al lloc correcte en la paraula a trobar.
- Quan s'esgoten els intents, el jugador podrà tancar el joc o triar l'opció "si" per tornar a començar.




